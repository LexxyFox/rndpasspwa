const CACHE_NAME = 'cache-v8'

const CACHE_ASSETS = [
	'index.html'
]

self.addEventListener('install', event => {
	self.skipWaiting()
	
	event.waitUntil(
		caches.open(CACHE_NAME).then(cache => {
			cache.addAll(CACHE_ASSETS)
		})
	)
})

const activate = async () => {
	for (const c of (await caches.keys())) 
		if (c !== CACHE_NAME) 
			await caches.delete(c)
}

self.addEventListener('activate', event => {
	event.waitUntil(activate())
})

const fetch_page = async req => {
	try {
		return await fetch(req)
	} catch {
		return await caches.match(req)
	}
}

self.addEventListener('fetch', event =>
	event.respondWith(fetch_page(event.request))
)
