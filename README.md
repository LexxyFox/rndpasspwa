## [https://lexxyfox.codeberg.page/rndpasspwa](https://lexxyfox.codeberg.page/rndpasspwa)

![Screenshot](https://lexxyfox.codeberg.page/rndpasspwa/_.png)

[![DON'T upload to GitHub](https://no-github.bitbucket.io/badge.svg)](https://no-github.bitbucket.io)

* Fully offline puesdo-random password generator
* Customisable allowed characters
* Downloadable PWA
* Actually cross platform
* No AI / LLMs
* No badware
* No compiling
* No credit cards
* No dependencies
* No email
* No evil
* No fat
* No GitHub
* No Google
* No keyloggers
* No malware
* No NFTs
* No NodeJS
* No scams
* No signups
* No soyware
* No spam
* No spyware
* No trackers
* No viruses

## Help wanted!

Is super ugly. Can make pretty? Thank uuu! <3
